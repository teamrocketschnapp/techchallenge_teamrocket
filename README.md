# README #

TU München

TechChallenge winter semester 2017 project.

Health diary allowing chronic pain patients to track their daily pain levels alongside factors such as sleep quality as well as medication intake times. This allows the attending medical practitioners to detect patterns and adapt therapy methods accordingly in close cooperation with their patients.




# painLess
Less attention. Less friction. Less pain.

Features:

- Track pain symptoms through a questionnaire
- Record medication intake dates
- Data access for medical practitioners


# team🚀 technology stack

Bitbucket:
https://bitbucket.org/teamrocketschnapp/techchallenge_teamrocket

Amazon Developer Dashboard:
https://developer.amazon.com

AWS:
https://aws.amazon.com/

CircleCI:
https://circleci.com/

APEX:
http://apex.run/

DynamoDB:
https://aws.amazon.com/dynamodb/

reDash:
https://redash.io/