const alexaSDK = require('alexa-sdk');
const awsSDK = require('aws-sdk');
const questions = require('./question');
const promisify = require('es6-promisify');
const docClient = new awsSDK.DynamoDB.DocumentClient();
console.log('Completed imports');

// convert callback style functions to promises
const dbPut = promisify(docClient.put, docClient);
const dbScan = promisify(docClient.scan, docClient);
const dbGet = promisify(docClient.get, docClient);
const dbDelete = promisify(docClient.delete, docClient);

const answersTable = 'Answers';
const medicationTable = 'Medication';

const TRACKER_STATES = {
    ONETOTENMODE: '_ONETOTENMODE', // Answering scale questions, 1 to 10.
    ONETOTHREEMODE: '_ONETOTHREEMODE', // Answering scale questions, 1 to 3.
    BINARYMODE: '_BINARYMODE', // Answering binary questions.
    START: '_STARTMODE', // Entry point, start the questionnaire.
    HELP: '_HELPMODE', // The user is asking for help.
};

const APP_ID = 'amzn1.ask.skill.1f7d7cd7-cd22-4be0-9c59-0cdbcd98ec50';

const languageStrings = {
    'de': {
        'translation': {
            'QUESTIONS': questions['QUESTIONS_DE_DE'],
            'START_UNHANDLED': ' Sie können jederzeit den Fragebogen beginnen, sagen Sie einfach "Starte Fragebogen".',
            'ASK_MESSAGE_START': 'Moechten Sie beginnen?',
            'REPEAT_QUESTION_MESSAGE': 'Wenn die letzte Frage wiederholt werden soll, sagen Sie "Wiederholen"',
            'STOP_MESSAGE': 'Möchten Sie mit dem Fragebogen fortfahren?',
            'HELP_MESSAGE': 'Ich gehe mit Ihnen einen Fragebogen durch. Antworten Sie auf meine Fragen mit einer Zahl auf der jeweils von mir beschriebenen Skala.',
            'HELP_REPROMPT': 'Antworten Sie auf meine Fragen mit einer Zahl auf der genannten Skala. Zum Wiederholen der Frage, sagen Sie "Wiederholen".',
            'NO_MESSAGE': 'OK, machen wir ein andermal weiter. Auf Wiedersehen!',
            'CANCEL_MESSAGE': 'OK, dann lassen Sie uns bald mal wieder sprechen.',
            'HELP_UNHANDLED': 'Sagen Sie "ja", um fortzufahren, oder "nein", um den Fragebogen zu beenden.',
            'TRACKER_UNHANDLED': 'Bitte beantworten Sie meine Frage.',
            'PILL_ENTRY': 'Ich habe mir aufgeschrieben, dass Sie gerade Ihre Medikamente genommen haben. ',
            'NEW_SESSION': 'Lass uns loslegen. ',
            'WELCOME_MESSAGE': 'Willkommen beim Gesundheitstagebuch! Sie können jederzeit den Fragebogen beginnen, sagen Sie einfach "Starte Fragebogen". Sie können mir auch mitteilen, dass Sie gerade Ihre Medikamente genommen haben.',
            'TELL_QUESTION_MESSAGE': '%s. Frage. %s ',
            'END_MESSAGE': 'Herzlichen Dank fuer die Beantwortung des Fragebogens.',
        },
    },
};

const newSessionHandler = {
    'LaunchRequest': function () {
        let speechOutput = this.t('WELCOME_MESSAGE');
        this.response.speak(speechOutput).listen(speechOutput);
        this.handler.state = TRACKER_STATES.START;
        this.emit(':responseReady');
    },
};

const startStateHandlers = alexaSDK.CreateStateHandler(TRACKER_STATES.START, {
    'StartQuestionnaire': function (newSession) {
        console.log('Fragebogen wird gestartet');
        const translatedQuestions = this.t('QUESTIONS');
        const questionnaire = populateQuestions(translatedQuestions);
        const currentQuestionIndex = 0;
        const spokenQuestion = Object.keys(translatedQuestions[questionnaire[currentQuestionIndex]])[0];
        let repromptText = this.t('TELL_QUESTION_MESSAGE', '1', spokenQuestion);
        let speechOutput = newSession ? this.t('NEW_SESSION') + repromptText: repromptText;

        Object.assign(this.attributes, {
            'spokenQuestion': spokenQuestion,
            'speechOutput': repromptText,
            'repromptText': repromptText,
            'currentQuestionIndex': currentQuestionIndex,
            'questions': questionnaire,
        });

        if (translatedQuestions[questionnaire[currentQuestionIndex]][Object.keys(translatedQuestions[questionnaire[currentQuestionIndex]])[0]][0] === 1) {
            console.log('switching to ONETOTENMODE');
            this.handler.state = TRACKER_STATES.ONETOTENMODE;
        } else if (translatedQuestions[questionnaire[currentQuestionIndex]][Object.keys(translatedQuestions[questionnaire[currentQuestionIndex]])[0]][0] === 2) {
            console.log('switching to ONETOTHREEMODE');
            this.handler.state = TRACKER_STATES.ONETOTHREEMODE;
        } else if (translatedQuestions[questionnaire[currentQuestionIndex]][Object.keys(translatedQuestions[questionnaire[currentQuestionIndex]])[0]][0] === 3) {
            console.log('switching to BINARYMODE');
            this.handler.state = TRACKER_STATES.BINARYMODE;
        }
        console.log(this.handler.state);
        this.response.speak(speechOutput).listen(repromptText);
        this.emit(':responseReady');
    },
    'BeginIntent': function () {
        this.handler.state = TRACKER_STATES.START;
        console.log('Lass uns loslegen.');
        this.emitWithState('StartQuestionnaire', true);
    },
    'PillenIntent': function () {
        pillEntry.call(this);
        this.response.speak(this.t('PILL_ENTRY') + this.t('START_UNHANDLED'));
        this.emit(':responseReady');
    },
    'AMAZON.StopIntent': function () {
        this.response.speak(this.t('NO_MESSAGE'));
        this.emit(':responseReady');
    },
    'AMAZON.CancelIntent': function () {
        this.response.speak(this.t('CANCEL_MESSAGE'));
        this.emit(':responseReady');
    },
    'AMAZON.HelpIntent': function () {
        this.handler.state = TRACKER_STATES.HELP;
        this.emitWithState('helpTheUser', true);
    },
    'Unhandled': function () {
        const speechOutput = this.t('START_UNHANDLED');
        this.response.speak(speechOutput).listen(speechOutput);
        this.emit(':responseReady');
    },
});

function populateQuestions(translatedQuestions) {
    const questionnaire = [];
    for (let i = 0; i < translatedQuestions.length; i++) {
        questionnaire.push(i);
    }
    return questionnaire;
}

const helpStateHandlers = alexaSDK.CreateStateHandler(TRACKER_STATES.HELP, {
    'helpTheUser': function (newSession) {
        const askMessage = newSession ? this.t('ASK_MESSAGE_START') : this.t('REPEAT_QUESTION_MESSAGE') + this.t('STOP_MESSAGE');
        const speechOutput = this.t('HELP_MESSAGE') + askMessage;
        const repromptText = this.t('HELP_REPROMPT') + askMessage;

        this.response.speak(speechOutput).listen(repromptText);
        this.emit(':responseReady');
    },
    'AMAZON.StartOverIntent': function () {
        this.handler.state = TRACKER_STATES.START;
        this.emitWithState('StartQuestionnaire', false);
    },
    'AMAZON.RepeatIntent': function () {
        const newSession = !(this.attributes['speechOutput'] && this.attributes['repromptText']);
        this.emitWithState('helpTheUser', newSession);
    },
    'AMAZON.HelpIntent': function () {
        const newSession = !(this.attributes['speechOutput'] && this.attributes['repromptText']);
        this.emitWithState('helpTheUser', newSession);
    },
    'AMAZON.YesIntent': function () {
        this.handler.state = TRACKER_STATES.START;
        this.emitWithState('StartQuestionnaire', false);
    },
    'AMAZON.NoIntent': function () {
        const speechOutput = this.t('NO_MESSAGE');
        this.response.speak(speechOutput);
        this.emit(':responseReady');
    },
    'AMAZON.StopIntent': function () {
        const speechOutput = this.t('STOP_MESSAGE');
        this.response.speak(speechOutput).listen(speechOutput);
        this.emit(':responseReady');
    },
    'AMAZON.CancelIntent': function () {
        this.response.speak(this.t('CANCEL_MESSAGE'));
        this.emit(':responseReady');
    },
    'Unhandled': function () {
        const speechOutput = this.t('HELP_UNHANDLED');
        this.response.speak(speechOutput).listen(speechOutput);
        this.emit(':responseReady');
    },
    'SessionEndedRequest': function () {
        console.log('Session ended in help state: ${this.event.request.reason}');
    },
});

function handleUserAnswer(type, answer, skip) {
    let speechOutput = '';
    let currentQuestionIndex = parseInt(this.attributes.currentQuestionIndex, 10);
    const translatedQuestions = this.t('QUESTIONS');
    const questionnaire = this.attributes.questions;

    if (!skip) {
        const { userId } = this.event.session.user;
        const entrytime = Date.now();

        const dynamoParams = {
          TableName: answersTable,
          Item: {
            entrytime: entrytime,
            user_id: userId,
            question: this.attributes.spokenQuestion,
            type: type,
            answer: answer
          }
        };
        console.log('Initialized dynamoParams');
        console.log(dynamoParams);

        dbPut(dynamoParams);
        console.log('Add item succeeded');
    }

    if (this.attributes['currentQuestionIndex'] == translatedQuestions.length - 1) {
        this.response.speak(this.t('END_MESSAGE'));
        this.emit(':responseReady');
    } else {
        currentQuestionIndex++;
        const questionIndexForSpeech = currentQuestionIndex + 1;
        const spokenQuestion = Object.keys(translatedQuestions[questionnaire[currentQuestionIndex]])[0];
        let speechOutput = this.t('TELL_QUESTION_MESSAGE', questionIndexForSpeech.toString(), spokenQuestion);
        let repromptText = this.t('TELL_QUESTION_MESSAGE', questionIndexForSpeech.toString(), spokenQuestion);

        Object.assign(this.attributes, {
            'spokenQuestion': spokenQuestion,
            'speechOutput': repromptText,
            'repromptText': repromptText,
            'currentQuestionIndex': currentQuestionIndex,
            'questions': questionnaire,
        });

        if (translatedQuestions[questionnaire[currentQuestionIndex]][Object.keys(translatedQuestions[questionnaire[currentQuestionIndex]])[0]][0] === 1) {
            console.log('switching to ONETOTENMODE');
            this.handler.state = TRACKER_STATES.ONETOTENMODE;
        } else if (translatedQuestions[questionnaire[currentQuestionIndex]][Object.keys(translatedQuestions[questionnaire[currentQuestionIndex]])[0]][0] === 2) {
            console.log('switching to ONETOTHREEMODE');
            this.handler.state = TRACKER_STATES.ONETOTHREEMODE;
        } else if (translatedQuestions[questionnaire[currentQuestionIndex]][Object.keys(translatedQuestions[questionnaire[currentQuestionIndex]])[0]][0] === 3) {
            console.log('switching to BINARYMODE');
            this.handler.state = TRACKER_STATES.BINARYMODE;
        }

        this.response.speak(speechOutput).listen(repromptText);
        this.emit(':responseReady');
    }
}

const oneToTenAnswerHandlers = alexaSDK.CreateStateHandler(TRACKER_STATES.ONETOTENMODE, {
    'AnswerIntentEins': function () {
        handleUserAnswer.call(this, 1, 1, false);
    },
    'AnswerIntentZwei': function () {
        handleUserAnswer.call(this, 1, 2, false);
    },
    'AnswerIntentDrei': function () {
        handleUserAnswer.call(this, 1, 3, false);
    },
    'AnswerIntentVier': function () {
        handleUserAnswer.call(this, 1, 4, false);
    },
    'AnswerIntentFuenf': function () {
        handleUserAnswer.call(this, 1, 5, false);
    },
    'AnswerIntentSechs': function () {
        handleUserAnswer.call(this, 1, 6, false);
    },
    'AnswerIntentSieben': function () {
        handleUserAnswer.call(this, 1, 7, false);
    },
    'AnswerIntentAcht': function () {
        handleUserAnswer.call(this, 1, 8, false);
    },
    'AnswerIntentNeun': function () {
        handleUserAnswer.call(this, 1, 9, false);
    },
    'AnswerIntentZehn': function () {
        handleUserAnswer.call(this, 1, 10, false);
    },
    'SkipIntent': function () {
        handleUserAnswer.call(this, 0, 0, true);
    },
    'BeginIntent' : function () {
        this.response.speak(this.attributes['repromptText']).listen(this.attributes['repromptText']);
        this.emit(':responseReady');
    },
    'PillenIntent': function () {
        pillEntry.call(this);
        this.response.speak(this.t('PILL_ENTRY'));
        this.handler.state = TRACKER_STATES.START;
        this.emit(':responseReady');
    },
    'AMAZON.RepeatIntent': function () {
        this.response.speak(this.attributes['speechOutput']).listen(this.attributes['repromptText']);
        this.emit(':responseReady');
    },
    'AMAZON.HelpIntent': function () {
        this.handler.state = TRACKER_STATES.HELP;
        this.emitWithState('helpTheUser', false);
    },
    'AMAZON.StopIntent': function () {
        this.handler.state = TRACKER_STATES.HELP;
        const speechOutput = this.t('STOP_MESSAGE');
        this.response.speak(speechOutput).listen(speechOutput);
        this.emit(':responseReady');
    },
    'AMAZON.CancelIntent': function () {
        this.response.speak(this.t('CANCEL_MESSAGE'));
        this.emit(':responseReady');
    },
    'Unhandled': function () {
        const speechOutput = this.t('TRACKER_UNHANDLED');
        this.response.speak(speechOutput).listen(speechOutput);
        this.emit(':responseReady');
    },
});

const oneToThreeAnswerHandlers = alexaSDK.CreateStateHandler(TRACKER_STATES.ONETOTHREEMODE, {
    'AnswerIntentEins': function () {
        handleUserAnswer.call(this, 2, 1, false);
    },
    'AnswerIntentZwei': function () {
        handleUserAnswer.call(this, 2, 2, false);
    },
    'AnswerIntentDrei': function () {
        handleUserAnswer.call(this, 2, 3, false);
    },
    'SkipIntent': function () {
        handleUserAnswer.call(this, 0, 0, true);
    },
    'BeginIntent' : function () {
        this.response.speak(this.attributes['repromptText']).listen(this.attributes['repromptText']);
        this.emit(':responseReady');
    },
    'PillenIntent': function () {
        pillEntry.call(this);
        this.response.speak(this.t('PILL_ENTRY'));
        this.handler.state = TRACKER_STATES.START;
        this.emit(':responseReady');
    },
    'AMAZON.RepeatIntent': function () {
        this.response.speak(this.attributes['speechOutput']).listen(this.attributes['repromptText']);
        this.emit(':responseReady');
    },
    'AMAZON.HelpIntent': function () {
        this.handler.state = TRACKER_STATES.HELP;
        this.emitWithState('helpTheUser', false);
    },
    'AMAZON.StopIntent': function () {
        this.handler.state = TRACKER_STATES.HELP;
        const speechOutput = this.t('STOP_MESSAGE');
        this.response.speak(speechOutput).listen(speechOutput);
        this.emit(':responseReady');
    },
    'AMAZON.CancelIntent': function () {
        this.response.speak(this.t('CANCEL_MESSAGE'));
        this.emit(':responseReady');
    },
    'Unhandled': function () {
        const speechOutput = this.t('TRACKER_UNHANDLED');
        this.response.speak(speechOutput).listen(speechOutput);
        this.emit(':responseReady');
    },
});

const binaryAnswerHandlers = alexaSDK.CreateStateHandler(TRACKER_STATES.BINARYMODE, {
    'AMAZON.YesIntent': function () {
        handleUserAnswer.call(this, 3, 1, false);
    },
    'AMAZON.NoIntent': function () {
        handleUserAnswer.call(this, 3, 0, false);
    },
    'SkipIntent': function () {
        handleUserAnswer.call(this, 0, 0, true);
    },
    'BeginIntent' : function () {
        this.response.speak(this.attributes['repromptText']).listen(this.attributes['repromptText']);
        this.emit(':responseReady');
    },
    'PillenIntent': function () {
        pillEntry.call(this);
        this.response.speak(this.t('PILL_ENTRY'));
        this.handler.state = TRACKER_STATES.START;
        this.emit(':responseReady');
    },
    'AMAZON.RepeatIntent': function () {
        this.response.speak(this.attributes['speechOutput']).listen(this.attributes['repromptText']);
        this.emit(':responseReady');
    },
    'AMAZON.HelpIntent': function () {
        this.handler.state = TRACKER_STATES.HELP;
        this.emitWithState('helpTheUser', false);
    },
    'AMAZON.StopIntent': function () {
        this.handler.state = TRACKER_STATES.HELP;
        const speechOutput = this.t('STOP_MESSAGE');
        this.response.speak(speechOutput).listen(speechOutput);
        this.emit(':responseReady');
    },
    'AMAZON.CancelIntent': function () {
        this.response.speak(this.t('CANCEL_MESSAGE'));
        this.emit(':responseReady');
    },
    'Unhandled': function () {
        const speechOutput = this.t('TRACKER_UNHANDLED');
        this.response.speak(speechOutput).listen(speechOutput);
        this.emit(':responseReady');
    },
});

function pillEntry() {
    const { userId } = this.event.session.user;
    const entrytime = Date.now();

    const dynamoParams = {
      TableName: medicationTable,
      Item: {
        entrytime: entrytime,
        user_id: userId,
        pills: 10
      }
    };
    console.log('Initialized dynamoParams');
    console.log(dynamoParams);

    dbPut(dynamoParams);
    console.log('Add item succeeded');
}

export default function (event, context) {
    var alexa = alexaSDK.handler(event, context);
    alexa.appId = APP_ID;
    alexa.resources = languageStrings;
    alexa.registerHandlers(newSessionHandler, startStateHandlers, helpStateHandlers, binaryAnswerHandlers, oneToTenAnswerHandlers, oneToThreeAnswerHandlers);
    alexa.execute();
}
