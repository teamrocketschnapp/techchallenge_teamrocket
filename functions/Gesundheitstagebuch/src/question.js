'use strict';

module.exports = {
    /**
     * When editing your questions pay attention to your punctuation. Make sure you use question marks or periods.
     * Question Types
     1 == scale 1 to 10
     2 == scale 1 to 3
     3 == binary
     */

    QUESTIONS_DE_DE: [
        {
            'Auf einer Skala von 1 (keine Schmerzen) bis 10 (starke Schmerzen), wie stark sind Ihre Schmerzen heute?': [
                1,
            ],
        },
        {
            'Auf einer Skala von 1 (schlecht) bis 3 (gut), wie bewerten Sie Ihren Stuhlgang heute?': [
                2,
            ],
        },
        {
            'Auf einer Skala von 1 bis 3, wie bewerten Sie Ihren Schlaf vergangene Nacht?': [
                2,
            ],
        },
        {
            'Auf einer Skala von 1 bis 3, wie bewerten Sie Ihr generelles Wohlbefinden heute?': [
                2,
            ],
        },
        {
            'Auf einer Skala von 1 bis 3, wie stark wurden Ihre Tagesaktivitaeten heute durch Ihre Schmerzen beeintraechtigt?': [
                2,
            ],
        },
    ],
};
